
TMultiLayerPerceptron *mlp;

void mlpEnu(Int_t ntrain=300) {
	gStyle->SetPalette(55);
	gEnv->SetValue("Hist.Binning.2D.x",100);
	gEnv->SetValue("Hist.Binning.2D.y",100);
	gRandom->SetSeed(0);
	
	TFile *f = TFile::Open("FASER_numu_W184.nt_forMLP.root");
	
	TTree *tree = (TTree *) f->Get("nt2");

//	char NNInputs[] = "slopeInvSum/1000,1/halfAngle/100,nChargedMulti**2/100,nGamma**2/100,1/angleLepton/100:7:5:Enu_true/1000";
	char NNInputs[] = "slopeInvSum/1000,1/halfAngle/100,nChargedMulti**2/100,nGamma**2/100,1/angleLepton/100,EgammaSum/1000,momLepton/1000,pChargedHadronSum/1000:10:5:Enu_true/1000";
	char Weight[]   = "1/Enu_true";
	
	// Define NN
	mlp = new TMultiLayerPerceptron(NNInputs, Weight,tree,
		"Entry$%2",
		"(Entry$+1)%2");
	//mlp->SetLearningMethod(TMultiLayerPerceptron::kSteepestDescent);
	//mlp->SetLearningMethod(TMultiLayerPerceptron::kBFGS);
	
	
	int onlyonce=0;
	
	
	if(onlyonce){
		mlp->Train(ntrain, "text,graph,update=10");
	} 
	else {
	
		double err=1000000;
		double errGoal=0.0129;
		int nevents = tree->GetEntries("(Entry$+1)%2");
		// Training
		// loop to get better training results
		while( err>errGoal){
			mlp->Train(ntrain, "text,graph,update=10");
			err = mlp->GetError(TMultiLayerPerceptron::kTest)/nevents;
			err = sqrt(err);
			printf("err = %lf (goal=%f)\n", err, errGoal);
		}
	}
	
		
	int mlpversion = 4;
	mlp->Export(Form("MLP_v%d", mlpversion),"c++");
	
	// Use TMLPAnalyzer to see what it looks for
	TCanvas* mlpa_canvas = new TCanvas("mlpa_canvas","Network analysis",0,300,1200,800);
	mlpa_canvas->Divide(3,2);
	
	TMLPAnalyzer ana(mlp);
	// Initialisation
	ana.GatherInformations();
	// output to the console
	ana.CheckNetwork();
	
	mlpa_canvas->cd(1);
	// shows how each variable influences the network
	ana.DrawDInputs();
	
	mlpa_canvas->cd(2);
	// shows the network structure
	mlp->Draw();
	
	
	
	
	
	TFile fout("out.root","recreate");
	
	float slopeInvSum, nCharged, nGamma, EgammaSum, Enu_true, angleLepton, momLepton, halfAngle, pChargedHadronSum;
	tree->SetBranchAddress("slopeInvSum", &slopeInvSum);
	tree->SetBranchAddress("nChargedMulti", &nCharged);
	tree->SetBranchAddress("nGamma", &nGamma);
	tree->SetBranchAddress("EgammaSum", &EgammaSum);
	tree->SetBranchAddress("angleLepton", &angleLepton);
	tree->SetBranchAddress("momLepton", &momLepton);
	tree->SetBranchAddress("Enu_true", &Enu_true);
	tree->SetBranchAddress("halfAngle", &halfAngle);
	tree->SetBranchAddress("pChargedHadronSum", &pChargedHadronSum);
	
	TNtuple *ntNN = new TNtuple("ntNN", "", "Enu_true:Enu_NN:Enu_kine");
	Double_t params[9];
	for (int i = 0; i < tree->GetEntries(); i++) {
		tree->GetEntry(i);
		
		
		if(nCharged<5) continue;

/*		params[0] = EgammaSum;
		params[1] = slopeInvSum;
		params[2] = nCharged;
		params[3] = nGamma;
		params[4] = angleLepton;
		params[5] = momLepton;
*/
/*
		params[0] = EgammaSum/1000;
		//params[1] = halfAngle;
		params[1] = slopeInvSum/1000;
		params[2] = nCharged;
		params[3] = nGamma;
		params[4] = angleLepton;
		params[5] = momLepton/1000;
*/

/*
		params[0] = (EgammaSum)/1000*(1+gRandom->Gaus(0,0.4));
		params[1] = (slopeInvSum)/1000;
		params[2] = (nCharged**2)/100;
		params[3] = (nGamma**2)/100;
		params[4] = (1/angleLepton)/1000;
*/

//		params[0] = (EgammaSum)/1000*(1+gRandom->Gaus(0,0.4));
		params[0] = (slopeInvSum)/1000;
		params[1] = (1/halfAngle)/100;
		params[2] = (nCharged**2)/100;
		params[3] = (nGamma**2)/100;
		params[4] = 1/angleLepton/100.;
		params[5] = (EgammaSum)/1000;
		params[6] = (momLepton)/1000;
		params[7] = (pChargedHadronSum)/1000;


		
		float Enu_NN = mlp->Evaluate(0,params);
		
		ntNN->Fill(Enu_true, Enu_NN*1000, momLepton+pChargedHadronSum+EgammaSum);
//		ntNN->Fill(Enu_true, Enu_NN);
	}
	
	mlpa_canvas->cd(3);
 	
 	gEnv->SetValue("Hist.Binning.2D.x",100);
	gEnv->SetValue("Hist.Binning.2D.y",100);
	
	ntNN->Draw("Enu_NN:Enu_true","Enu_true<5000","colz");

	mlpa_canvas->cd(4);
	
	ntNN->Draw("(Enu_NN-Enu_true)/Enu_true","Enu_true<5000","");

	mlp->DumpWeights("weights.txt");

	mlpa_canvas->cd(5);
	
	TProfile *hprof = new TProfile("hprof","",100,0,5000,"S");
	ntNN->Draw("(Enu_NN-Enu_true)/Enu_true:Enu_true >>hprof","","");
	
	ntNN->Write("ntNN");
	
	
	fout.Close();
}
	
	
/*	TH2F * hTraining = new TH2F ("hTraining","MLP traning sample", 300,-50,150,200,-50,150);//150,0,150,150,0,150);
	hTraining->SetXTitle("P_{DS} Truth");
	hTraining->SetYTitle("P_{DS} Reconstructed");
	
	TH2F *hTest = hTraining->Clone("hTest");
	hTest->SetTitle("MLP test sample");
	
	TH2F *hSlopeOnly = hTraining->Clone("hSlopeOnly");
	hSlopeOnly->SetTitle("Slope only");
	
	nt->Draw("p_ds_NN:p_ds >>hTraining","Entry$%2","col");
	
	
	mlpa_canvas->cd(4);
	nt->Draw("p_ds_NN:p_ds >>hTest","(Entry$+1)%2","col");
	
	mlpa_canvas->cd(5);
	
	tree->Draw("0.200942/dt_ds2tau:p_ds >>hSlopeOnly","n_tau_dau_charged==1","col");
	
	mlpa_canvas->cd(6);
	
	// dP over P
	TH1F *hdPoP_NN = new TH1F ("hdPoP_NN","dP over P", 40, -1, 3);
	TH1F *hdPoP_SlopeOnly = hdPoP_NN->Clone("hdPoP_SlopeOnly");
	
	
	hdPoP_SlopeOnly->SetLineColor(kRed+2);
	
	nt->Draw("(p_ds_NN-p_ds)/p_ds >>hdPoP_NN","(Entry$+1)%2");
	
	hdPoP_NN->Fit("gaus");
	
	tree->Draw("(0.200942/dt_ds2tau-p_ds)/p_ds >>hdPoP_SlopeOnly","n_tau_dau_charged==1&&(Entry$+1)%2","same");
	
	
	mlpa_canvas->Print("mlpDsMom.temp.png");
	
	TFile fout("DsMomentumEstimatorMLP.temp.root","recreate");
	mlp->Write("DsMomentumEstimatorMLP");
	fout.Close();
*/	
	
	

